import cppyy
import pkg_resources

# Make sure, if we are distributed in archive form (e.g. an egg), that the
# headers are extracted. Therefore we need to look for the folder name first.
HEADERS_DIR = pkg_resources.resource_filename(
    "emberplus", "upstream/libember/Headers/ember")

# Scan the headers.
cppyy.include(HEADERS_DIR + '/Ember.hpp')

# Load the library.
cppyy.load_library(
    pkg_resources.resource_filename("emberplus", "libember-shared.so"))

# For now, until we pythonize this thing, export the top-level namespace.
libember = cppyy.gbl.libember

# Keep the module namespace clear.
del cppyy, pkg_resources
