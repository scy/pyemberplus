from contextlib import contextmanager
import os
import pathlib
import setuptools
import setuptools.command.build_clib
import shutil
import subprocess

CMAKE_DIR = "cmakebuild"
PACKAGE_DIR = "emberplus"
UPSTREAM_SOURCE_DIR = PACKAGE_DIR + "/upstream"
LIBEMBER_SOURCE_DIR = UPSTREAM_SOURCE_DIR + "/libember"

# `with`-compatible chdir, from <https://stackoverflow.com/a/24176022/417040>.
@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(newdir)
    try:
        yield
    finally:
        os.chdir(prevdir)

def get_readme():
    with open("README.md", "r") as fh:
        return fh.read()

def have_libember_source():
    return os.path.isdir(LIBEMBER_SOURCE_DIR)

def libember_sdist_files():
    # TODO: Maybe filter out some really unneeded files.
    return [str(path) for path in pathlib.Path(UPSTREAM_SOURCE_DIR).glob("**/*") if path.is_file()]

def build_libember(outdir):
    if not have_libember_source():
        raise RuntimeError("Cannot find libember sources at {0}. When building from Git, make sure you have the submodule checked out.".format(LIBEMBER_SOURCE_DIR))

    # Create CMake output folder.
    pathlib.Path(CMAKE_DIR).mkdir(exist_ok=True)

    # Run CMake.
    with cd(CMAKE_DIR):
        subprocess.run(["cmake", "../" + UPSTREAM_SOURCE_DIR], check=True)
        subprocess.run(["cmake", "--build", ".", "--target", "ember-shared"], check=True)

    # This is the part where it gets ugly. I couldn't for the life of me find
    # out how to include all the upstream code in the sdist, but only headers
    # and the compiled shared library in the wheel, so I'm copying around and
    # deleting things all willy-nilly here. But it seems to build a working
    # wheel, so there's that.
    with cd(outdir + "/upstream"):
        to_remove = []
        ember_headers = [path for path in pathlib.Path(".").glob("libember/**/*.hpp")]
        ember_headers += [path for path in pathlib.Path(".").glob("libember/**/*.ipp")]
        for path in pathlib.Path(".").glob("**/*"):
            if path in ember_headers or str(path) == "libember" or (path.is_dir() and str(path).startswith("libember/Headers")):
                continue
            to_remove.append(str(path))
        # TODO: This is Unix-only, rewrite in Python.
        subprocess.run(["rm", "-rf"] + to_remove, check=True)
    shutil.copy2(CMAKE_DIR + "/libember/libember-shared.so", outdir)


class BuildClibCommand(setuptools.command.build_clib.build_clib):

    def build_libraries(self, libraries):
        build_libember("build/lib/emberplus")


if __name__ == "__main__":
    setuptools.setup(
        name="pyemberplus",
        version="0.1.0",
        author="Tim Weber",
        author_email="scy-pyemberplus-setuppy@scy.name",
        description="Python wrapper around libember, which implements the Ember+ protocol by Lawo",
        long_description=get_readme(),
        long_description_content_type="text/markdown",
        url="https://codeberg.org/scy/pyemberplus",
        packages=setuptools.find_packages(),
        classifiers=[
            "Development Status :: 3 - Alpha",
            "Intended Audience :: Developers",
            "License :: OSI Approved :: MIT License",
            "Natural Language :: English",
            "Operating System :: POSIX",
            "Programming Language :: Python :: 3",
            "Topic :: Software Development :: Libraries :: Python Modules",
        ],
        install_requires=["cppyy"],
        python_requires=">=3.6",
        cmdclass={
            "build_clib": BuildClibCommand,
        },
        libraries=[("libember", {"sources": libember_sdist_files()})],
        include_package_data=True,
    )
